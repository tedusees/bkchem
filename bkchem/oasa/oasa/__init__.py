# --------------------------------------------------------------------------
#     This file is part of OASA - a free chemical python library
#     Copyright (C) 2003-2008 Beda Kosata <beda@zirael.org>

#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     Complete text of GNU GPL can be found in the file gpl.txt in the
#     main directory of the program

# --------------------------------------------------------------------------

from bkchem.oasa.oasa import transform
from bkchem.oasa.oasa import transform3d
from bkchem.oasa.oasa import geometry
from bkchem.oasa.oasa import stereochemistry
from bkchem.oasa.oasa import svg_out
from bkchem.oasa.oasa import subsearch
from bkchem.oasa.oasa import name_database
from bkchem.oasa.oasa import oasa_exceptions
from bkchem.oasa.oasa import chem_vertex
from bkchem.oasa.oasa import query_atom
from bkchem.oasa.oasa import config
from bkchem.oasa.oasa import periodic_table
from bkchem.oasa.oasa import linear_formula
from bkchem.oasa.oasa import graph
from bkchem.oasa.oasa import cdml
from bkchem.oasa.oasa import inchi
from bkchem.oasa.oasa import molfile
from bkchem.oasa.oasa import coords_optimizer
from bkchem.oasa.oasa import coords_generator
from bkchem.oasa.oasa import smiles
from bkchem.oasa.oasa import molecule
from bkchem.oasa.oasa import bond
from bkchem.oasa.oasa import atom
import sys
if not (sys.version_info[0] > 2 or (sys.version_info[0] == 2 and sys.version_info[1] >= 3)):
    raise ImportError(
        "Python version %d.%d is lower than 2.3 which is needed by OASA" % sys.version_info[0:2])


atom = atom.atom
molecule = molecule.molecule
bond = bond.bond
query_atom = query_atom.query_atom
chem_vertex = chem_vertex.chem_vertex

all = ['atom', 'bond', 'molecule', 'smiles', 'coords_generator', 'molfile', 'inchi', 'graph',
       "linear_formula", 'periodic_table', 'config', 'coords_optimizer', 'chem_vertex',
       'query_atom', 'oasa_exceptions', 'name_database', "subsearch", "svg_out",
       'stereochemistry', 'geometry', 'transform', 'transform3d']

try:
    from . import cairo_out
except:
    CAIRO_AVAILABLE = False
else:
    all.append("cairo_out")
    CAIRO_AVAILABLE = True

# inchi_key
try:
    from . import inchi_key
except Exception as e:
    #print >> sys.stderr, "Module inchi_key could not be loaded - inchi_key related features will be disabled\nSee the error message for more info:\n  %s" % e
    INCHI_KEY_AVAILABLE = False
else:
    all.append("inchi_key")
    INCHI_KEY_AVAILABLE = True

# structure_database requires sqlite
try:
    from . import structure_database
except Exception as e:
    #print >> sys.stderr, "Module structure_database could not be loaded - structure_database related features will be disabled\nSee the error message for more info:\n  %s" % e
    STRUCTURE_DATABASE_AVAILABLE = False
else:
    all.append("structure_database")
    STRUCTURE_DATABASE_AVAILABLE = True

# pybel
try:
    from . import pybel_bridge
except Exception as e:
    #print >> sys.stderr, "The 'pybel_bridge' python module could not be loaded - oasa-pybel integration will be disabled\nSee the error message for more info:\n  %s" % e
    PYBEL_AVAILABLE = False
else:
    all.append("pybel_bridge")
    PYBEL_AVAILABLE = True


__all__ = all
