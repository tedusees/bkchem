import os
import sys
import dbm
from bkchem.oasa.oasa import inchi as inchimod
from bkchem.oasa.oasa import inchi_key


class Config:
    database_file = os.path.abspath(os.path.join(
        os.path.dirname(__file__), "names.db"))


def normalize_inchi(inchi):
    if inchi.startswith("InChI="):
        return inchi[6:]
    else:
        return inchi


def compound_to_database_string(c):
    c['inchi'] = normalize_inchi(c['inchi'])
    c['inchikey'] = inchi_key.key_from_inchi(c['inchi'])
    return "%(inchikey)s %(cid)s ### %(name)s\n" % c


def database_string_to_compound(line):
    a, name = line.split("###")
    inchi, cid = a.split()
    inchikey = inchi_key.key_from_inchi(inchi)
    return {'inchikey': inchikey.strip(), 'cid': cid.strip(), 'name': name.strip()}


def mydb_to_gdbm(infilename, outfilename):
    infile = file(infilename, "r")
    base = dbm.open(outfilename, "n")
    for line in infile:
        rec = database_string_to_compound(line)
        base[rec['inchikey']] = rec['cid'] + " " + rec['name']


def get_compound_from_database(inchikey, database_file=None):
    #inchi = normalize_inchi( inchi)
    for fname in (database_file, Config.database_file):
        if fname and os.path.exists(fname):
            break
    else:
        raise Exception("Name database not found")
    base = dbm.open(fname)
    if inchikey in base:
        cid, name = base[inchikey].split(" ", 1)
        return {'inchikey': inchikey, 'cid': cid, 'name': name}
    else:
        return None


def name_molecule(mol, database_file=None):
    """tries to find name for an OASA molecule in the database,
    it requires InChI generation to work"""
    key = inchimod.generate_inchi_key(mol)
    return get_compound_from_database(key, database_file=database_file)


if __name__ == "__main__":
    import sys
    if len(sys.argv) > 1:
        fname = sys.argv[1]
        if os.path.exists(fname):
            try:
                mydb_to_gdbm(fname, Config.database_file)
            except:
                print(
                    "given file must be a text file with one compound per line in format 'InChI CID ### name'")
        else:
            print(
                "you must supply a valid filename to update the database or no argument for a test to run")
    else:
        print((get_compound_from_database("IJDNQMDRQITEOD-UHFFFAOYSA-N")))
