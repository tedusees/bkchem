# -------------------- HELP MESSAGES FOR MODES --------------------

# DRAW

draw_mode_single = ("Click on atom to add a new single bond, click on bond to change its order (or change direction)\nShift+Click on a double bond to move the position of the second line")
draw_mode_fixed = ("Length of the drawn bond is fixed to a preset value")
draw_mode_freestyle = ("Freestyle drawing - no bond length or angle restrictions are applied")

# ARROW

arrow_mode_fixed = ("Length of the drawn arrow (arrow part) is fixed to a preset value")
arrow_mode_freestyle = ("Freestyle drawing - no arrow length or angle restrictions are applied")
arrow_mode_anormal = ("Arrow is composed of straight lines")
arrow_mode_spline = ("Arrow is drawn as a b-spline")

# ROTATION

rotate_mode_2D = ("Drag an atom or a bond to 2D-rotate a molecule around its geometrical center")
rotate_mode_3D = ("Drag an atom or a bond to 3D-rotate a molecule around its geometrical center\nPlease note that it is not possible to add a template to an atom with z-coordinate other than 0")
rotate_mode_fixsomething = ("Select a bond and then start rotating - the molecule will rotate around the selected bond\nBy holding shift, you will trigger rotation only of one part of the molecule.")

# VECTOR

vector_mode_square = ("Click and drag to draw the corresponding shape")
vector_mode_rect = ("Click and drag to draw the corresponding shape")
vector_mode_oval = ("Click and drag to draw the corresponding shape")
vector_mode_circle = ("Click and drag to draw the corresponding shape")
vector_mode_polygon = ("Each left-button click creates one point of the polygon, right click closes it")


# MARKS

mark_mode_radical = ("Click an atom to add a radical mark to it, it will be removed in case it is already present")
mark_mode_biradical = ("Click an atom to add a biradical mark to it, it will be removed in case it is already present")
mark_mode_electronpair = ("Click an atom to add a free electron pair mark to it, it will be removed in case it is already present")
mark_mode_plusincircle = ("Click an atom to add a plus mark to it, it will be removed in case it is already present")
mark_mode_minusincircle = ("Click an atom to add a minus mark to it, it will be removed in case it is already present")


# BOND ALIGN

bond_align_mode_tovert = ("Click a bond or two atoms to align the specified line into vertical position")
bond_align_mode_tohoriz = ("Click a bond or two atoms to align the specified line into horizontal position")
bond_align_mode_invertthrough = ("Click an atom or bond to perform an inversion of the molecule through this atom (center of the bond)")
bond_align_mode_mirrorthrough = ("Click a bond or two atoms to mirror the molecule through the specified line")

# -------------------- OTHER MESSAGES --------------------


about_text = ("""BKchem was written by Beda Kosata.
This version of BKchem is made by Teoakn Duran Demircan.

BKchem is free software and is distributed under GNU GPL.
BKchem is provided as is without warranty of any kind.

Among other things GNU GPL allows you to study, modify
and redistribute BKchem as long as you do it under GNU GPL.""")


no_pmw_text = ("UNABLE TO IMPORT PMW\n\nSorry, but BKchem relies too heavily on Pmw to be usable without it. Please install Pmw and try again.")

no_oasa_text = ("UNABLE TO IMPORT OASA\n\nSorry, but from version 0.10, BKchem uses the OASA library internally and therefor cannot run without it.")

low_python_version_text = ("Python version too low!\n\nFrom version 0.10 BKchem needs Python 2.3 or higher to run properly. Sorry for the inconvenience, but you would need to upgrade Python in order to run BKchem.")

splash_text = ("BKchem is starting.\n\nUnfortunately, the splash image was not found.")

standards_differ_text = ("This CDML document was created using different standard values than you are currently using. To retain the same drawing style it could be more convenient to use these new values for this file. Your global preferences will be unaffected. \n\nDo you want set these new standard values?")

usage_text = ("Usage: bkchem [options] [filenames]\n\nOptions:\n\n-h, --help\t\tshow this help message and exit")
