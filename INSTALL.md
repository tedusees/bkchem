# Installation guide

# Prerequisites
---
To run bkchem, you must have Python 3.x installed in your system.
## Single directory deployment
---
No installation is needed to run bkchem. This means that you can run the program
directly after unpacking the downloaded sources. Simply run the bkchem.py located in the `bkchem-X.Y.Z/bkchem` directory, where `X, Y, Z` are version numbers of bkchem release.

Example:
```
on Unix do
>cd "the dir where you have downloaded the bkchem package"
>tar -xzf bkchem-X.X.X.tgz -C "the dir where you want to unpack bkchem"
>cd "the dir where you have unpacked bkchem"/bkchem-X.Y.Z/bkchem
>python bkchem.py
```
## More info
---
Documentation will be put in `/prefix/share/doc/bkchem`. Finally a sh-script `/prefix/bin/bkchem` will be created so that you can run bkchem from anywhere you want.

Any comments or reports on the installation process are especially welcome as this is very hard to test thoroughly on a single machine.