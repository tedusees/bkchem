# bkchem-port

This project is a modified Python 3 port of the 2D molecular drawing program bkchem by Beda Kosata and licensed under GPL 2.0.

For installation instructions, see the INSTALL file in the main directory.